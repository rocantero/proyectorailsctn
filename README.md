Lab. Ruby on Rails 
=========

Este proyecto pertenece al curso ***Laboratorio Ruby on Rails*** del tercer curso turno tarde, de la Especialidad de Informática del Colegio Técnico Nacional de Asunción.

> El objetivo del curso es recorrer los conceptos de aplicaciones web y el paradigma de desarrollo MVC, mediante una aplicación web construida con la ayuda de Ruby on Rails 4.

This text you see here is *actually* written in Markdown! To get a feel for Markdown's syntax, type some text into the left window and watch the results in the right.  

Año
----
2.014

Instrucciones
-----------
Para participar en el proyecto, se debe:
- clonar el repositorio del proyecto
- crear un ***branch*** para poder modificar el proyecto de forma separada al resto
- modificar este archivo (README.md) indicando los participantes en el branch
- seguir las guías e instrucciones para ir avanzando en el proyecto
- subir los cambios al branch creado

### Clonar el repositorio
    git clone https://rocantero@bitbucket.org/rocantero/proyectorailsctn.git/ 
### Crear un nuevo branch
    git -b [nombre_del_branch]
    
Luego de clonar o crear un repositorio, el branch por defecto es el **master**. Los integrantes del grupo deberán entonces hacer **checkout** del branch creado para poder trabajar en el mismo proyecto.
### Checkout de un branch
    git checkout [nombre_del_branch]

##### Configure Plugins. Instructions in following README.md files

* plugins/dropbox/README.md
* plugins/github/README.md
* plugins/googledrive/README.md

## Contacto
> Rodrigo Cantero

> cantero.rodrigo91@gmail.com

> 0983-461-999