Rails.application.routes.draw do
  get 'inicio/index'

  root 'inicio#index'
  # Rutas de recurso Ejemplo
  resources :ejemplos 
  
  # Rutas para Tipos
  resources :tipos

  # Rutas para Elementos y Comentarios
  resources :elementos do
    resources :comentarios
  end
end
