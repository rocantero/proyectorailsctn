require 'test_helper'

class ElementosControllerTest < ActionController::TestCase
  test "should get listar" do
    get :listar
    assert_response :success
  end

  test "should get nuevo" do
    get :nuevo
    assert_response :success
  end

  test "should get crear" do
    get :crear
    assert_response :success
  end

end
