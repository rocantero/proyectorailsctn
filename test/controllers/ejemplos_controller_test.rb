require 'test_helper'

class EjemplosControllerTest < ActionController::TestCase
  setup do
    @ejemplo = ejemplos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ejemplos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ejemplo" do
    assert_difference('Ejemplo.count') do
      post :create, ejemplo: { nombre: @ejemplo.nombre, numero: @ejemplo.numero }
    end

    assert_redirected_to ejemplo_path(assigns(:ejemplo))
  end

  test "should show ejemplo" do
    get :show, id: @ejemplo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ejemplo
    assert_response :success
  end

  test "should update ejemplo" do
    patch :update, id: @ejemplo, ejemplo: { nombre: @ejemplo.nombre, numero: @ejemplo.numero }
    assert_redirected_to ejemplo_path(assigns(:ejemplo))
  end

  test "should destroy ejemplo" do
    assert_difference('Ejemplo.count', -1) do
      delete :destroy, id: @ejemplo
    end

    assert_redirected_to ejemplos_path
  end
end
