class Elemento < ActiveRecord::Base
  belongs_to :tipo
	has_many :comentarios
  mount_uploader :imagen, ImagenUploader
end
