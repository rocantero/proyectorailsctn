json.array!(@elementos) do |elemento|
  json.extract! elemento, :id, :nombre, :numero
  json.url elemento_url(elemento, format: :json)
end
