class CreateComentarios < ActiveRecord::Migration
  def change
    create_table :comentarios do |t|
      t.string :contenido
      t.string :email_autor
      t.string :nombre_autor
      t.date :fecha_envio
      t.integer :elemento_id

      t.timestamps
    end
  end
end
